# vectator.js
vectator.js is a JavaScript library for efficiently rendering vector graphics
in the Vectator file format. It doesn't actually render any graphics; it
simply interprets the code. It's up to graphics back-ends to do the rendering.

**What's with the name?** Well, I considered "Vectoria", but that seemed too
obvious. "Vectator" is a Latin word that could almost have ended up meaning
the same thing as "vector"... but didn't. I like it because it's conjugated
just like my screen name, "Permutator".
