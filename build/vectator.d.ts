declare module Vectator {
    class Stack<E> {
        items: E[];
        constructor(items?: E[] | Stack<E>, copy?: boolean);
        push(...items: E[]): void;
        pop(): E;
        pops(amount: number): E[];
        peek(): E;
        peeks(amount: number): E[];
    }
}
declare module Vectator {
    class Context extends Stack<any> {
        private vars;
        constructor(vars?: {
            [name: string]: any;
        });
        pushVar(name: string, ...items: any[]): void;
        popVar(name: string): any;
        popsVar(name: string, amount: number): any[];
        peekVar(name: string): any;
        peeksVar(name: string, amount: number): any[];
    }
}
declare module Vectator {
    function exec<S>(ops: Op<S>[], ctx: Context, subject: S): void;
    interface Op<S> {
        (ctx: Context, subject: S): void;
    }
}
declare module Vectator {
    interface Library<S> {
        get(name: string): Op<S>;
    }
    class ObjectLibrary<S> implements Library<S> {
        lib: {
            [name: string]: Op<S>;
        };
        supers: Library<S>[];
        constructor(lib: {
            [name: string]: Op<S>;
        }, ...supers: Library<S>[]);
        get(name: string): Op<S>;
    }
}
declare module Vectator {
    class Program<S> {
        ops: Op<S>[];
        constructor(src: string, library: Library<S>);
        constructor(ops: Op<S>[], copy?: boolean);
        constructor();
        loadString(src: string, library: Library<S>): void;
        execute(subject?: S, context?: Context): void;
        execute(subject?: S, vars?: {
            [name: string]: any;
        }): void;
    }
}
declare module Vectator {
    module Libraries {
        let base: ObjectLibrary<any>;
    }
}
