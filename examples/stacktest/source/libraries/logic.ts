/// <reference path="../../../../build/vectator.d.ts" />

module Vectator {
  "use strict";
  export module Libraries {
    export let logic = new ObjectLibrary<any>({
      'not': (ctx: Context) => ctx.push(!ctx.pop()),
      'or': (ctx: Context) => ctx.push(ctx.pop() || ctx.pop()),
      'and': (ctx: Context) => ctx.push(ctx.pop() && ctx.pop()),
      'xor': (ctx: Context) => ctx.push(!ctx.pop() !== !ctx.pop())
    });
  }
}
