/// <reference path="../../../../build/vectator.d.ts" />

module Vectator {
  "use strict";
  export module Libraries {
    export let controlFlow = new ObjectLibrary<any>({
      // condition, block
      'if': (ctx: Context, subject: any) => {
        let doIt: boolean = ctx.pop();
        let block: Op<any>[] = ctx.pop();
        if(doIt)
          exec(block, ctx, subject);
      },
      // condition, block for true, block for false
      'if-else': (ctx: Context, subject: any) => {
        let doIt: boolean = ctx.pop();
        let yes: Op<any>[] = ctx.pop(), no: Op<any>[] = ctx.pop();
        exec(doIt ? yes : no, ctx, subject);
      },
      // condition (in the form of a block), block
      'while': (ctx: Context, subject: any) => {
        let cond: Op<any>[] = ctx.pop(), block: Op<any>[] = ctx.pop();
        while(true) {
          exec(cond, ctx, subject);
          if(!ctx.pop()) break;
          exec(block, ctx, subject);
        }
      },
      // array, block
      'for-each': (ctx: Context, subject: any) => {
        let arr: any[] = ctx.pop(), block: Op<any>[] = ctx.pop();
        for(let i = 0; i < arr.length; ++i) {
          ctx.push(arr[i]);
          exec(block, ctx, subject);
        }
      },
      // start, end (inclusive), step, block
      'for-range': (ctx: Context, subject: any) => {
        let start: number = ctx.pop(),
            end: number = ctx.pop(),
            step: number = ctx.pop(),
            block: Op<any>[] = ctx.pop();
        if((end > start) !== (step > 0) || step === 0) return;
        if(step < 0)
          for(let i = start; i >= end; i += step) {
            ctx.push(i);
            exec(block, ctx, subject);
          }
        else
          for(let i = start; i <= end; i += step) {
            ctx.push(i);
            exec(block, ctx, subject);
          }
      }
    });
  }
}
