/// <reference path="../../../../build/vectator.d.ts" />

module Vectator {
  "use strict";
  export module Libraries {
    // left-hand side, right-hand side
    export let arithmetic = new ObjectLibrary<any>({
      '=': (ctx: Context) => ctx.push(ctx.pop() === ctx.pop()),
      '!=': (ctx: Context) => ctx.push(ctx.pop() !== ctx.pop()),
      '>': (ctx: Context) => ctx.push(ctx.pop() > ctx.pop()),
      '<': (ctx: Context) => ctx.push(ctx.pop() < ctx.pop()),
      '>=': (ctx: Context) => ctx.push(ctx.pop() >= ctx.pop()),
      '<=': (ctx: Context) => ctx.push(ctx.pop() <= ctx.pop()),
      '+': (ctx: Context) => ctx.push(ctx.pop() + ctx.pop()),
      '-': (ctx: Context) => ctx.push(ctx.pop() - ctx.pop()),
      '*': (ctx: Context) => ctx.push(ctx.pop() * ctx.pop()),
      '/': (ctx: Context) => ctx.push(ctx.pop() / ctx.pop()),
      '//': (ctx: Context) => ctx.push((ctx.pop() / ctx.pop()) | 0)
    });
  }
}
