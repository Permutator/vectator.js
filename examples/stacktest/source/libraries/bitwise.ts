/// <reference path="../../../../build/vectator.d.ts" />

module Vectator {
  "use strict";
  export module Libraries {
    // left-hand side, right-hand side
    export let bitwise = new ObjectLibrary<any>({
      'bnot': (ctx: Context) => ctx.push(~ctx.pop()),
      'bor': (ctx: Context) => ctx.push(ctx.pop() | ctx.pop()),
      'band': (ctx: Context) => ctx.push(ctx.pop() & ctx.pop()),
      'bxor': (ctx: Context) => ctx.push(ctx.pop() ^ ctx.pop()),
      'lshift': (ctx: Context) => ctx.push(ctx.pop() << ctx.pop()),
      // rshifts: signed; rshiftu: unsigned
      'rshifts': (ctx: Context) => ctx.push(ctx.pop() >> ctx.pop()),
      'rshiftu': (ctx: Context) => ctx.push(ctx.pop() >>> ctx.pop())
    });
  }
}
