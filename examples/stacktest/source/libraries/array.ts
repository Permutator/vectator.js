/// <reference path="../../../../build/vectator.d.ts" />

module Vectator {
  "use strict";
  export module Libraries {
    function sortf(ctx: Context, subject: any, copy: boolean) {
      let arr: any[] = ctx.pop();
      if(copy) arr = arr.slice();
      let func: Op<any>[] = ctx.pop();
      arr.sort((a: any, b: any) => {
        ctx.push(b, a);
        exec(func, ctx, subject);
        return ctx.pop();
      });
      if(copy) ctx.push(arr);
    }
    // concat and length also work on strings
    // following the Ruby convention: ones with !'s operate in-place
    // these ones actually do pop their arguments from the stack, so
    // you'll probably want to use dup
    export let array = new ObjectLibrary<any>({
      'concat': (ctx: Context) => ctx.push(ctx.pop().concat(ctx.pop())),
      'concat!': (ctx: Context) => Array.prototype.push.apply(ctx.pop(), ctx.pop()),
      // length of array, array elements in reverse order
      'pack': (ctx: Context) => ctx.push(ctx.pops(ctx.pop())),
      // returns length of array on top, followed by elements
      'unpack': (ctx: Context) => {
        let arr: any[] = ctx.pop();
        Context.prototype.push.apply(ctx, arr);
        ctx.push(arr.length);
      },
      // this really does pop its argument off the stack, so beware!
      'length': (ctx: Context) => ctx.push(ctx.pop().length),
      // this also pops its argument off the stack!
      'copy': (ctx: Context) => ctx.push(ctx.pop().slice()),
      /* sorting: 'f' functions take sorting functions as their second
         arguments. these sorting functions are passed two values; they
         must pop them both and push a positive number if the first one
         should be put after the second, etc. this will reeaally mess
         up the stack if you have any leaks! */
      'sort': (ctx: Context) => {
        let arr: any[] = ctx.pop().slice();
        arr.sort();
        ctx.push(arr);
      },
      'sort!': (ctx: Context) => ctx.pop().sort(),
      'sortf': (ctx: Context, subject: any) => sortf(ctx, subject, true),
      'sortf!': (ctx: Context, subject: any) => sortf(ctx, subject, false),
      // reversal
      'reverse': (ctx: Context) => {
        let arr: any[] = ctx.pop().slice();
        arr.reverse();
        ctx.push(arr);
      },
      'reverse!': (ctx: Context) => ctx.pop().reverse()
    });
  }
}
