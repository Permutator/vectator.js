/// <reference path="../../../../build/vectator.d.ts" />

module Vectator {
  "use strict";
  export module Libraries {
    // left-hand side, right-hand side
    export let math = new ObjectLibrary<any>({
      // rounding
      'round': (ctx: Context) => ctx.push(Math.round(ctx.pop())),
      'floor': (ctx: Context) => ctx.push(Math.floor(ctx.pop())),
      'ceil': (ctx: Context) => ctx.push(Math.ceil(ctx.pop())),
      // misc
      'sqrt': (ctx: Context) => ctx.push(Math.sqrt(ctx.pop())),
      'abs': (ctx: Context) => ctx.push(Math.cos(ctx.pop())),
      // trig conversion
      'to-deg': (ctx: Context) => ctx.push(ctx.pop() * 180 / Math.PI),
      'to-rad': (ctx: Context) => ctx.push(ctx.pop() * Math.PI / 180),
      // trig
      'sin': (ctx: Context) => ctx.push(Math.sin(ctx.pop())),
      'cos': (ctx: Context) => ctx.push(Math.cos(ctx.pop())),
      'tan': (ctx: Context) => ctx.push(Math.tan(ctx.pop())),
      'asin': (ctx: Context) => ctx.push(Math.asin(ctx.pop())),
      'acos': (ctx: Context) => ctx.push(Math.acos(ctx.pop())),
      'atan': (ctx: Context) => ctx.push(Math.atan(ctx.pop())),
      // exponents and logarithms
      'exp': (ctx: Context) => ctx.push(Math.exp(ctx.pop())),
      'ln': (ctx: Context) => ctx.push(Math.log(ctx.pop())),
      'pow': (ctx: Context) => ctx.push(Math.pow(ctx.pop(), ctx.pop())),
      'log': (ctx: Context) => ctx.push(Math.log(ctx.pop()) / Math.log(ctx.pop())),
      // constants
      'PI': (ctx: Context) => ctx.push(Math.PI),
      'E': (ctx: Context) => ctx.push(Math.E),
      // min/max
      'min': (ctx: Context) => ctx.push(Math.min(ctx.pop(), ctx.pop())),
      'max': (ctx: Context) => ctx.push(Math.max(ctx.pop(), ctx.pop())),
      // clamp: value to clamp, lower bound, upper bound
      'clamp': (ctx: Context) => ctx.push(Math.min(Math.max(ctx.pop(), ctx.pop()), ctx.pop())),
      // random; randint takes one argument: maximum value, exclusive
      'random': (ctx: Context) => ctx.push(Math.random()),
      'rand-int': (ctx: Context) => ctx.push((Math.random() * ctx.pop()) | 0)
    });
  }
}
