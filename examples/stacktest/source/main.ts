/// <reference path="../../../build/vectator.d.ts" />
/// <reference path="./libraries/all.ts" />

let input = <HTMLTextAreaElement>document.getElementById('input');
let button = <HTMLButtonElement>document.getElementById('execute');
let output = <HTMLElement>document.getElementById('output');

let library = new Vectator.ObjectLibrary<string[]>({
  'print': (ctx: Vectator.Context, subject: string[]) => subject[0] += ctx.pop() + '<br />'
}, Vectator.Libraries.base, Vectator.Libraries.arithmetic, Vectator.Libraries.array,
   Vectator.Libraries.bitwise, Vectator.Libraries.controlFlow, Vectator.Libraries.logic,
   Vectator.Libraries.math);

button.addEventListener('mouseup', () => {
  let prog = new Vectator.Program<string[]>(input.value, library);
  let out = [''];
  prog.execute(out);
  output.innerHTML = out[0];
});
