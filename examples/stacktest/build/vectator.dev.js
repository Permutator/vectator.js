/* vectator.js v1.0.1
 *
 * Copyright (c) 2015, Permutator
 * Licensed under the MIT license:
 * http://opensource.org/licenses/MIT
 *
 * The source code, written in TypeScript, is available on Bitbucket:
 * https://bitbucket.org/Permutator/vectator.js
 */
var Vectator;
(function (Vectator) {
    "use strict";
    var Stack = (function () {
        function Stack(items, copy) {
            if (items) {
                var elements = Array.isArray(items) ? items : items.items;
                this.items = copy ? elements.slice() : elements;
            }
            else {
                this.items = [];
            }
        }
        Stack.prototype.push = function () {
            Array.prototype.push.apply(this.items, arguments);
        };
        Stack.prototype.pop = function () {
            var out = this.items[this.items.length - 1];
            --this.items.length;
            return out;
        };
        Stack.prototype.pops = function (amount) {
            return this.items.splice(this.items.length - amount, amount);
        };
        Stack.prototype.peek = function () {
            return this.items[this.items.length - 1];
        };
        Stack.prototype.peeks = function (amount) {
            return this.items.slice(-amount);
        };
        return Stack;
    })();
    Vectator.Stack = Stack;
})(Vectator || (Vectator = {}));
/// <reference path="./Stack.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Vectator;
(function (Vectator) {
    "use strict";
    var Context = (function (_super) {
        __extends(Context, _super);
        function Context(vars) {
            _super.call(this);
            this.vars = {};
            if (vars)
                for (var name_1 in vars)
                    if (vars.hasOwnProperty(name_1))
                        this.vars[name_1] = new Vectator.Stack([vars[name_1]]);
        }
        Context.prototype.pushVar = function (name) {
            var items = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                items[_i - 1] = arguments[_i];
            }
            if (name in this.vars)
                Vectator.Stack.prototype.push.apply(this.vars[name], items);
            else
                this.vars[name] = new Vectator.Stack(items);
        };
        Context.prototype.popVar = function (name) {
            if (name in this.vars)
                return this.vars[name].pop();
        };
        Context.prototype.popsVar = function (name, amount) {
            if (name in this.vars)
                return this.vars[name].pops(amount);
        };
        Context.prototype.peekVar = function (name) {
            if (name in this.vars)
                return this.vars[name].peek();
        };
        Context.prototype.peeksVar = function (name, amount) {
            if (name in this.vars)
                return this.vars[name].peeks(amount);
        };
        return Context;
    })(Vectator.Stack);
    Vectator.Context = Context;
})(Vectator || (Vectator = {}));
/// <reference path="./Context.ts" />
var Vectator;
(function (Vectator) {
    "use strict";
    function exec(ops, ctx, subject) {
        for (var i = 0; i < ops.length; ++i) {
            ops[i](ctx, subject);
        }
    }
    Vectator.exec = exec;
})(Vectator || (Vectator = {}));
/// <reference path="./Op.ts" />
var Vectator;
(function (Vectator) {
    "use strict";
    var ObjectLibrary = (function () {
        function ObjectLibrary(lib) {
            var supers = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                supers[_i - 1] = arguments[_i];
            }
            this.lib = lib;
            this.supers = supers;
        }
        ObjectLibrary.prototype.get = function (name) {
            if (this.lib[name])
                return this.lib[name];
            for (var i = 0; i < this.supers.length; ++i) {
                var out = this.supers[i].get(name);
                if (out)
                    return out;
            }
        };
        return ObjectLibrary;
    })();
    Vectator.ObjectLibrary = ObjectLibrary;
})(Vectator || (Vectator = {}));
/// <reference path="./preamble.ts" />
/// <reference path="./Stack.ts" />
/// <reference path="./Context.ts" />
/// <reference path="./Library.ts" />
/// <reference path="./Op.ts" />
var Vectator;
(function (Vectator) {
    "use strict";
    var Program = (function () {
        function Program(prog, other) {
            if (prog === undefined) {
                this.ops = [];
            }
            else if (typeof prog === 'string') {
                this.ops = [];
                this.loadString(prog, other);
            }
            else {
                this.ops = other ? prog.slice() : prog;
            }
        }
        Program.prototype.loadString = function (src, library) {
            function isSpace(str, ind) {
                return /\s/.test(str[ind]);
            }
            function mfPlain(val) {
                return function (ctx) { return ctx.push(val); };
            }
            function mfPop(name) {
                return function (ctx) { return ctx.push(ctx.popVar(name)); };
            }
            function mfPush(name) {
                return function (ctx) { return ctx.pushVar(name, ctx.pop()); };
            }
            function mfPeek(name) {
                return function (ctx) { return ctx.push(ctx.peekVar(name)); };
            }
            function mfPopAway(name) {
                return function (ctx) { return ctx.popVar(name); };
            }
            var ops = this.ops, opsStack = new Vectator.Stack();
            for (var i = 0; i < src.length; ++i) {
                if (i >= src.length)
                    return;
                while (isSpace(src, i))
                    if (++i >= src.length)
                        return;
                var start = i;
                var command = null;
                if (src[i] === '"') {
                    while (++i < src.length && src[i] !== '"')
                        if (src[i] === '\\')
                            ++i;
                    var val = JSON.parse(src.substring(start, ++i));
                    command = mfPlain(val);
                }
                else {
                    while (++i < src.length && !isSpace(src, i)) { }
                    var str = src.substring(start, i);
                    var initial = str[0];
                    if (/\d/.test(initial) || (initial === '-' && str.length > 1)) {
                        var num = parseFloat(str);
                        command = mfPlain(num);
                    }
                    else {
                        if (str.length > 1) {
                            var rest = str.substr(1);
                            switch (initial) {
                                case '<':
                                    command = mfPop(rest);
                                    break;
                                case '>':
                                    command = mfPush(rest);
                                    break;
                                case '*':
                                    command = mfPeek(rest);
                                    break;
                                case '~':
                                    command = mfPopAway(rest);
                                    break;
                                case "'":
                                    command = mfPlain(rest);
                                    break;
                                case '#': {
                                    if (rest.length === 4) {
                                        rest = '';
                                        for (var j = 1; j <= 4; ++j)
                                            rest += str[j] + str[j];
                                        str = '#' + rest;
                                    }
                                    if (rest.length === 8) {
                                        if (rest.substr(6, 2).toLowerCase() === 'ff') {
                                            str = str.substr(0, 7);
                                        }
                                        else {
                                            str = 'rgba(';
                                            for (var j = 0; j < 6; j += 2) {
                                                str += parseInt(rest.substr(j, 2), 16) + ',';
                                            }
                                            str += (parseInt(rest.substr(6, 2), 16) / 255).toFixed(3) + ')';
                                        }
                                    }
                                    command = mfPlain(str);
                                    break;
                                }
                                case '?':
                                    if (rest === 'false' || rest === 'no' || rest === '0')
                                        command = function (ctx) { return ctx.push(false); };
                                    else
                                        command = function (ctx) { return ctx.push(true); };
                                    break;
                                default: command = library.get(str);
                            }
                        }
                        else {
                            switch (initial) {
                                case "'":
                                    command = function (ctx) { return ctx.push(''); };
                                    break;
                                case '(':
                                    opsStack.push(ops);
                                    ops = [];
                                    break;
                                case ')': {
                                    command = mfPlain(ops);
                                    ops = opsStack.pop();
                                    break;
                                }
                                default: command = library.get(str);
                            }
                        }
                    }
                }
                if (command !== null)
                    ops.push(command);
            }
        };
        Program.prototype.execute = function (subject, context) {
            context = context || new Vectator.Context();
            if (context.prototype !== Vectator.Context.prototype)
                context = new Vectator.Context(context);
            Vectator.exec(this.ops, context, subject);
        };
        return Program;
    })();
    Vectator.Program = Program;
})(Vectator || (Vectator = {}));
/// <reference path="./Library.ts" />
var Vectator;
(function (Vectator) {
    "use strict";
    var Libraries;
    (function (Libraries) {
        Libraries.base = new Vectator.ObjectLibrary({
            'dup': function (ctx) { return ctx.push(ctx.peek()); },
            'pop': function (ctx) { return ctx.pop(); },
            'exec': function (ctx, subject) {
                Vectator.exec(ctx.pop(), ctx, subject);
            }
        });
    })(Libraries = Vectator.Libraries || (Vectator.Libraries = {}));
})(Vectator || (Vectator = {}));
/// <reference path="./preamble.ts" />
/// <reference path="./Program.ts" />
/// <reference path="./Libraries.ts" />
