/// <reference path="../../../../build/vectator.d.ts" />
var Vectator;
(function (Vectator) {
    "use strict";
    var Libraries;
    (function (Libraries) {
        // left-hand side, right-hand side
        Libraries.arithmetic = new Vectator.ObjectLibrary({
            '=': function (ctx) { return ctx.push(ctx.pop() === ctx.pop()); },
            '!=': function (ctx) { return ctx.push(ctx.pop() !== ctx.pop()); },
            '>': function (ctx) { return ctx.push(ctx.pop() > ctx.pop()); },
            '<': function (ctx) { return ctx.push(ctx.pop() < ctx.pop()); },
            '>=': function (ctx) { return ctx.push(ctx.pop() >= ctx.pop()); },
            '<=': function (ctx) { return ctx.push(ctx.pop() <= ctx.pop()); },
            '+': function (ctx) { return ctx.push(ctx.pop() + ctx.pop()); },
            '-': function (ctx) { return ctx.push(ctx.pop() - ctx.pop()); },
            '*': function (ctx) { return ctx.push(ctx.pop() * ctx.pop()); },
            '/': function (ctx) { return ctx.push(ctx.pop() / ctx.pop()); },
            '//': function (ctx) { return ctx.push((ctx.pop() / ctx.pop()) | 0); }
        });
    })(Libraries = Vectator.Libraries || (Vectator.Libraries = {}));
})(Vectator || (Vectator = {}));
/// <reference path="../../../../build/vectator.d.ts" />
var Vectator;
(function (Vectator) {
    "use strict";
    var Libraries;
    (function (Libraries) {
        function sortf(ctx, subject, copy) {
            var arr = ctx.pop();
            if (copy)
                arr = arr.slice();
            var func = ctx.pop();
            arr.sort(function (a, b) {
                ctx.push(b, a);
                Vectator.exec(func, ctx, subject);
                return ctx.pop();
            });
            if (copy)
                ctx.push(arr);
        }
        // concat and length also work on strings
        // following the Ruby convention: ones with !'s operate in-place
        // these ones actually do pop their arguments from the stack, so
        // you'll probably want to use dup
        Libraries.array = new Vectator.ObjectLibrary({
            'concat': function (ctx) { return ctx.push(ctx.pop().concat(ctx.pop())); },
            'concat!': function (ctx) { return Array.prototype.push.apply(ctx.pop(), ctx.pop()); },
            // length of array, array elements in reverse order
            'pack': function (ctx) { return ctx.push(ctx.pops(ctx.pop())); },
            // returns length of array on top, followed by elements
            'unpack': function (ctx) {
                var arr = ctx.pop();
                Vectator.Context.prototype.push.apply(ctx, arr);
                ctx.push(arr.length);
            },
            // this really does pop its argument off the stack, so beware!
            'length': function (ctx) { return ctx.push(ctx.pop().length); },
            // this also pops its argument off the stack!
            'copy': function (ctx) { return ctx.push(ctx.pop().slice()); },
            /* sorting: 'f' functions take sorting functions as their second
               arguments. these sorting functions are passed two values; they
               must pop them both and push a positive number if the first one
               should be put after the second, etc. this will reeaally mess
               up the stack if you have any leaks! */
            'sort': function (ctx) {
                var arr = ctx.pop().slice();
                arr.sort();
                ctx.push(arr);
            },
            'sort!': function (ctx) { return ctx.pop().sort(); },
            'sortf': function (ctx, subject) { return sortf(ctx, subject, true); },
            'sortf!': function (ctx, subject) { return sortf(ctx, subject, false); },
            // reversal
            'reverse': function (ctx) {
                var arr = ctx.pop().slice();
                arr.reverse();
                ctx.push(arr);
            },
            'reverse!': function (ctx) { return ctx.pop().reverse(); }
        });
    })(Libraries = Vectator.Libraries || (Vectator.Libraries = {}));
})(Vectator || (Vectator = {}));
/// <reference path="../../../../build/vectator.d.ts" />
var Vectator;
(function (Vectator) {
    "use strict";
    var Libraries;
    (function (Libraries) {
        // left-hand side, right-hand side
        Libraries.bitwise = new Vectator.ObjectLibrary({
            'bnot': function (ctx) { return ctx.push(~ctx.pop()); },
            'bor': function (ctx) { return ctx.push(ctx.pop() | ctx.pop()); },
            'band': function (ctx) { return ctx.push(ctx.pop() & ctx.pop()); },
            'bxor': function (ctx) { return ctx.push(ctx.pop() ^ ctx.pop()); },
            'lshift': function (ctx) { return ctx.push(ctx.pop() << ctx.pop()); },
            // rshifts: signed; rshiftu: unsigned
            'rshifts': function (ctx) { return ctx.push(ctx.pop() >> ctx.pop()); },
            'rshiftu': function (ctx) { return ctx.push(ctx.pop() >>> ctx.pop()); }
        });
    })(Libraries = Vectator.Libraries || (Vectator.Libraries = {}));
})(Vectator || (Vectator = {}));
/// <reference path="../../../../build/vectator.d.ts" />
var Vectator;
(function (Vectator) {
    "use strict";
    var Libraries;
    (function (Libraries) {
        Libraries.controlFlow = new Vectator.ObjectLibrary({
            // condition, block
            'if': function (ctx, subject) {
                var doIt = ctx.pop();
                var block = ctx.pop();
                if (doIt)
                    Vectator.exec(block, ctx, subject);
            },
            // condition, block for true, block for false
            'if-else': function (ctx, subject) {
                var doIt = ctx.pop();
                var yes = ctx.pop(), no = ctx.pop();
                Vectator.exec(doIt ? yes : no, ctx, subject);
            },
            // condition (in the form of a block), block
            'while': function (ctx, subject) {
                var cond = ctx.pop(), block = ctx.pop();
                while (true) {
                    Vectator.exec(cond, ctx, subject);
                    if (!ctx.pop())
                        break;
                    Vectator.exec(block, ctx, subject);
                }
            },
            // array, block
            'for-each': function (ctx, subject) {
                var arr = ctx.pop(), block = ctx.pop();
                for (var i = 0; i < arr.length; ++i) {
                    ctx.push(arr[i]);
                    Vectator.exec(block, ctx, subject);
                }
            },
            // start, end (inclusive), step, block
            'for-range': function (ctx, subject) {
                var start = ctx.pop(), end = ctx.pop(), step = ctx.pop(), block = ctx.pop();
                if ((end > start) !== (step > 0) || step == 0)
                    return;
                if (step < 0)
                    for (var i = start; i >= end; i += step) {
                        ctx.push(i);
                        Vectator.exec(block, ctx, subject);
                    }
                else
                    for (var i = start; i <= end; i += step) {
                        ctx.push(i);
                        Vectator.exec(block, ctx, subject);
                    }
            }
        });
    })(Libraries = Vectator.Libraries || (Vectator.Libraries = {}));
})(Vectator || (Vectator = {}));
/// <reference path="../../../../build/vectator.d.ts" />
var Vectator;
(function (Vectator) {
    "use strict";
    var Libraries;
    (function (Libraries) {
        Libraries.logic = new Vectator.ObjectLibrary({
            'not': function (ctx) { return ctx.push(!ctx.pop()); },
            'or': function (ctx) { return ctx.push(ctx.pop() || ctx.pop()); },
            'and': function (ctx) { return ctx.push(ctx.pop() && ctx.pop()); },
            'xor': function (ctx) { return ctx.push(!ctx.pop != !ctx.pop()); }
        });
    })(Libraries = Vectator.Libraries || (Vectator.Libraries = {}));
})(Vectator || (Vectator = {}));
/// <reference path="../../../../build/vectator.d.ts" />
var Vectator;
(function (Vectator) {
    "use strict";
    var Libraries;
    (function (Libraries) {
        // left-hand side, right-hand side
        Libraries.math = new Vectator.ObjectLibrary({
            // rounding
            'round': function (ctx) { return ctx.push(Math.round(ctx.pop())); },
            'floor': function (ctx) { return ctx.push(Math.floor(ctx.pop())); },
            'ceil': function (ctx) { return ctx.push(Math.ceil(ctx.pop())); },
            // misc
            'sqrt': function (ctx) { return ctx.push(Math.sqrt(ctx.pop())); },
            'abs': function (ctx) { return ctx.push(Math.cos(ctx.pop())); },
            // trig conversion
            'to-deg': function (ctx) { return ctx.push(ctx.pop() * 180 / Math.PI); },
            'to-rad': function (ctx) { return ctx.push(ctx.pop() * Math.PI / 180); },
            // trig
            'sin': function (ctx) { return ctx.push(Math.sin(ctx.pop())); },
            'cos': function (ctx) { return ctx.push(Math.cos(ctx.pop())); },
            'tan': function (ctx) { return ctx.push(Math.tan(ctx.pop())); },
            'asin': function (ctx) { return ctx.push(Math.asin(ctx.pop())); },
            'acos': function (ctx) { return ctx.push(Math.acos(ctx.pop())); },
            'atan': function (ctx) { return ctx.push(Math.atan(ctx.pop())); },
            // exponents and logarithms
            'exp': function (ctx) { return ctx.push(Math.exp(ctx.pop())); },
            'ln': function (ctx) { return ctx.push(Math.log(ctx.pop())); },
            'pow': function (ctx) { return ctx.push(Math.pow(ctx.pop(), ctx.pop())); },
            'log': function (ctx) { return ctx.push(Math.log(ctx.pop()) / Math.log(ctx.pop())); },
            // constants
            'PI': function (ctx) { return ctx.push(Math.PI); },
            'E': function (ctx) { return ctx.push(Math.E); },
            // min/max
            'min': function (ctx) { return ctx.push(Math.min(ctx.pop(), ctx.pop())); },
            'max': function (ctx) { return ctx.push(Math.max(ctx.pop(), ctx.pop())); },
            // clamp: value to clamp, lower bound, upper bound
            'clamp': function (ctx) { return ctx.push(Math.min(Math.max(ctx.pop(), ctx.pop()), ctx.pop())); },
            // random; randint takes one argument: maximum value, exclusive
            'random': function (ctx) { return ctx.push(Math.random()); },
            'rand-int': function (ctx) { return ctx.push((Math.random() * ctx.pop()) | 0); }
        });
    })(Libraries = Vectator.Libraries || (Vectator.Libraries = {}));
})(Vectator || (Vectator = {}));
/// <reference path="./arithmetic.ts" />
/// <reference path="./array.ts" />
/// <reference path="./bitwise.ts" />
/// <reference path="./controlFlow.ts" />
/// <reference path="./logic.ts" />
/// <reference path="./math.ts" />
/// <reference path="../../../build/vectator.d.ts" />
/// <reference path="./libraries/all.ts" />
var input = document.getElementById('input');
var button = document.getElementById('execute');
var output = document.getElementById('output');
var library = new Vectator.ObjectLibrary({
    'print': function (ctx, subject) { return subject[0] += ctx.pop() + '<br />'; }
}, Vectator.Libraries.base, Vectator.Libraries.arithmetic, Vectator.Libraries.array, Vectator.Libraries.bitwise, Vectator.Libraries.controlFlow, Vectator.Libraries.logic, Vectator.Libraries.math);
button.addEventListener('mouseup', function () {
    var prog = new Vectator.Program(input.value, library);
    var out = [''];
    prog.execute(out);
    output.innerHTML = out[0];
});
