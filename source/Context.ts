/// <reference path="./Stack.ts" />

module Vectator {
  "use strict";
  export class Context extends Stack<any> {
    private vars: {[name: string]: Stack<any>};
    constructor(vars?: {[name: string]: any}) {
      super();
      this.vars = {};
      if(vars) for(let name in vars)
        if(vars.hasOwnProperty(name))
          this.vars[name] = new Stack([vars[name]]);
    }
    pushVar(name: string, ...items: any[]): void {
      if(name in this.vars) Stack.prototype.push.apply(this.vars[name], items);
      else this.vars[name] = new Stack(items);
    }
    popVar(name: string): any {
      if(name in this.vars) return this.vars[name].pop();
    }
    popsVar(name: string, amount: number): any[] {
      if(name in this.vars) return this.vars[name].pops(amount);
    }
    peekVar(name: string): any {
      if(name in this.vars) return this.vars[name].peek();
    }
    peeksVar(name: string, amount: number): any[] {
      if(name in this.vars) return this.vars[name].peeks(amount);
    }
  }
}
