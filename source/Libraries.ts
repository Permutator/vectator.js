/// <reference path="./Library.ts" />

module Vectator {
  "use strict";
  export module Libraries {
    export let base = new ObjectLibrary<any>({
      'dup': (ctx: Context) => ctx.push(ctx.peek()),
      'pop': (ctx: Context) => ctx.pop(),
      'exec': (ctx: Context, subject: any) => {
        exec(ctx.pop(), ctx, subject);
      }
    });
  }
}
