/// <reference path="./Context.ts" />

module Vectator {
  "use strict";
  export function exec<S>(ops: Op<S>[], ctx: Context, subject: S) {
    for(let i = 0; i < ops.length; ++i) {
      ops[i](ctx, subject);
    }
  }
  export interface Op<S> {
    (ctx: Context, subject: S): void;
  }
}
