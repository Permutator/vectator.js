/// <reference path="./Op.ts" />

module Vectator {
  "use strict";
  export interface Library<S> {
    get(name: string): Op<S>;
  }
  export class ObjectLibrary<S> implements Library<S> {
    lib: {[name: string]: Op<S>};
    supers: Library<S>[];
    constructor(lib: {[name: string]: Op<S>}, ...supers: Library<S>[]) {
      this.lib = lib;
      this.supers = supers;
    }
    get(name: string): Op<S> {
      if(this.lib[name]) return this.lib[name];
      for(let i = 0; i < this.supers.length; ++i) {
        let out = this.supers[i].get(name);
        if(out) return out;
      }
    }
  }
}
