module Vectator {
  "use strict";
  export class Stack<E> {
    items: E[];
    constructor(items?: E[] | Stack<E>, copy?: boolean) {
      if(items) {
        let elements = Array.isArray(items) ? <E[]>items : (<Stack<E>>items).items;
        this.items = copy ? elements.slice() : elements;
      } else {
        this.items = [];
      }
    }
    push(...items: E[]): void;
    push(): void {
      Array.prototype.push.apply(this.items, arguments);
    }
    pop(): E {
      let out = this.items[this.items.length-1];
      --this.items.length;
      return out;
    }
    pops(amount: number): E[] {
      return this.items.splice(this.items.length-amount, amount);
    }
    peek(): E {
      return this.items[this.items.length-1];
    }
    peeks(amount: number): E[] {
      return this.items.slice(-amount);
    }
  }
}
