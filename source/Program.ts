/// <reference path="./preamble.ts" />
/// <reference path="./Stack.ts" />
/// <reference path="./Context.ts" />
/// <reference path="./Library.ts" />
/// <reference path="./Op.ts" />

module Vectator {
  "use strict";
  export class Program<S> {
    ops: Op<S>[];
    constructor(src: string, library: Library<S>);
    constructor(ops: Op<S>[], copy?: boolean);
    constructor();
    constructor(prog?: any, other?: any) {
      if(prog === undefined) {
        this.ops = [];
      } else if(typeof prog === 'string') {
        this.ops = [];
        this.loadString(<string>prog, other)
      } else {
        this.ops = other ? prog.slice() : prog;
      }
    }
    loadString(src: string, library: Library<S>) {
      function isSpace(str: string, ind: number) {
        return /\s/.test(str[ind]);
      }
      
      function mfPlain(val: any) {
        return (ctx: Context) => ctx.push(val);
      }
      function mfPop(name: string) {
        return (ctx: Context) => ctx.push(ctx.popVar(name));
      }
      function mfPush(name: string) {
        return (ctx: Context) => ctx.pushVar(name, ctx.pop());
      }
      function mfPeek(name: string) {
        return (ctx: Context) => ctx.push(ctx.peekVar(name));
      }
      function mfPopAway(name: string) {
        return (ctx: Context) => ctx.popVar(name);
      }
      
      let ops = this.ops, opsStack = new Stack<Op<S>[]>();
      for(let i = 0; i < src.length; ++i) {
        if(i >= src.length) return;
        while(isSpace(src, i)) if(++i >= src.length) return;
        let start = i;
        let command: Op<S> = null;
        if(src[i] === '"') {
          while(++i < src.length && src[i] !== '"') if(src[i] === '\\') ++i;
          let val: string = JSON.parse(src.substring(start, ++i));
          command = mfPlain(val);
        } else {
          while(++i < src.length && !isSpace(src, i)) {  }
          let str = src.substring(start, i);
          let initial = str[0];
          if(/\d/.test(initial) || (initial === '-' && str.length > 1)) {
            let num = parseFloat(str);
            command = mfPlain(num);
          } else {
            if(str.length > 1) {
              let rest = str.substr(1);
              switch(initial) {
                case '<':
                  command = mfPop(rest); break;
                case '>':
                  command = mfPush(rest); break;
                case '*':
                  command = mfPeek(rest); break;
                case '~':
                  command = mfPopAway(rest); break;
                case "'":
                  command = mfPlain(rest); break;
                case '#': {
                  if(rest.length === 4) {
                    rest = '';
                    for(let j = 1; j <= 4; ++j) rest += str[j]+str[j];
                    str = '#'+rest;
                  }
                  if(rest.length === 8) {
                    if(rest.substr(6, 2).toLowerCase() === 'ff') {
                      str = str.substr(0, 7);
                    } else {
                      str = 'rgba(';
                      for(let j = 0; j < 6; j += 2) {
                        str += parseInt(rest.substr(j, 2), 16) + ',';
                      }
                      str += (parseInt(rest.substr(6, 2), 16) / 255).toFixed(3) + ')';
                    }
                  }
                  command = mfPlain(str);
                  break;
                }
                case '?':
                  if(rest === 'false' || rest === 'no' || rest === '0')
                    command = (ctx: Context) => ctx.push(false);
                  else
                    command = (ctx: Context) => ctx.push(true);
                  break;
                default: command = library.get(str);
              }
            } else {
              switch(initial) {
                case "'":
                  command = (ctx: Context) => ctx.push(''); break;
                case '(':
                  opsStack.push(ops); ops = []; break;
                case ')': {
                  command = mfPlain(ops);
                  ops = opsStack.pop();
                  break;
                }
                default: command = library.get(str);
              }
            }
          }
        }
        if(command !== null) ops.push(command);
      }
    }
    execute(subject?: S, context?: Context): void;
    execute(subject?: S, vars?: {[name: string]: any}): void;
    execute(subject?: S, context?: any): void {
      context = context || new Context();
      if(context.prototype !== Context.prototype)
        context = new Context(context);
      exec(this.ops, context, subject);
    }
  }
}
