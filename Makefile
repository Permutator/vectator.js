TSC=tsc -d
MAIN=source/vectator.ts
BUILD=build
OUT=$(BUILD)/vectator.js
DEVOUT=$(BUILD)/vectator.dev.js

.PHONY: all clean dev vectator

all: vectator

clean:
	rm -rf $(BUILD)/*

dev:
	$(TSC) --out $(DEVOUT) $(MAIN)
	mv -f $(BUILD)/vectator.dev.d.ts $(BUILD)/vectator.d.ts

vectator: clean dev
	cp -f preamble.js $(OUT)
	uglifyjs $(DEVOUT) >> $(OUT)
